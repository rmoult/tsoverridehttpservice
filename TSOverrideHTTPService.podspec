
Pod::Spec.new do |s|

s.name         = "TSOverrideHTTPService"
s.version      = "0.0.2"
s.summary      = "TSOverrideHTTPService gives easy access to alter HTTP responses in tests"
s.author       = "Richard Moult"
s.source_files = 'TSOverrideHTTPServiceTests/Classes/*.{h,m}'
s.requires_arc = true
s.platform     = :ios, '7.0'
s.source       = {:git => "https://bitbucket.org/rmoult/tsoverridehttpservice.git", :tag=> '0.0.2'}

end
