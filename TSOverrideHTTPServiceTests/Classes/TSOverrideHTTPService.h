//
//  Created by Richard Moult on 24/07/2013.
//  Copyright (c) 2015 trickysquirrel. All rights reserved.
//
//  How To Use:
//  For this class to override the network responses for testing purposes you will first need to call
//
//  [TSOverrideHTTPService setUp];
//
//  followed by any other static functions in this class to set up the responses
//  Don't forget to disable this behavour using once the test have been completed
//
//  given that an error and http files are mapped to the same URL the error takes presidence
//
//  [TSOverrideHTTPService tearDown];
//
//  Its good practive to call the above functions in the test setUp and tearDown methods



#import <Foundation/Foundation.h>


typedef void (^URLRequestedBlock)(NSURLRequest *URLRequest);



@interface TSOverrideHTTPService : NSURLProtocol


/*
 *  @abstract   Call before setting any URL mapping
 *
 *  @discussion This function registers this class for NSURLProtocol
 *
 */

+ (void)setUp;


/*
 *  @abstract   Call to remove any URL mapping
 *
 *  @discussion This function un registers this class for NSURLProtocol
 *              Forgets any URL mapping to errors
 *              Forgets any URL mapping to files
 *
 */

+ (void)tearDown;


/*
 *  @abstract   Override the network response with data from a resource file
 *
 *  @discussion Use this function to override the http request URL with the data contained with the file.
 *              Any previous set response or error will be removed
 *              This file must strictly adhear to standard http 1.0 response.
 *
 *  @param regex: pattern to match request URL
 *  @param fileName: fileName
 *  @param ofType: file name extension
 */

+ (void)setUpRequestURLRegex:(NSString *)regex mappedToRawHttpFileNamed:(NSString *)fileName ofType:(NSString *)ofType;



/*
 *  @abstract   Used to mapped a URL to a specific error
 *
 *  @discussion Use this function to map an error to a specific URL.
 *              The URL absolute string is used to map.
 *              This will take presidence over any mapped http files to that same URL
 *              Will only take affect if setUpAnyResponsesWithError is currently not in use.
 *
 *  @param regex: pattern to match request URL
 *  @param error: error to be returned in network request
 */

+ (void)setUpRequestRegex:(NSString *)regex mappedToResponseError:(NSError *)error;



/*
 *  @abstract   Used to un-map a URL to a specific file or error
 *
 *  @discussion Use this function to un-map an error or file to a specific URL.
 *              The URL mapping will be forgotten.
 *              Any mapped http files will now be returned.
 *
 *  @param regex: pattern to match request URL
 */

+ (void)removeMappedResponseForRequestURLRegex:(NSString *)regex;



/*
 *  @abstract   On any network request the error will be always returned
 *
 *  @discussion Use this function to pass back a NSError on the next network request regardless of the URL.
 *              Any previous errors to this function will be forgotten.
 *              Any specific errors mapped to URLs will be ignored
 *              Any http response files mapped to URLs will be ignored
 *
 *  @param error: error to be returned in network request
 */

+ (void)setUpAnyResponsesWithError:(NSError *)error;



/*
 *  @abstract   Removes the functionality set up in setUpAnyResponsesWithError
 *
 *  @discussion Call this function to cancel the behavour set up in setUpAnyResponsesWithError.
 *              Any specific errors mapped to URLs will now take presidence
 *              Any specific http files mapped to URLs will now response if specific errors do not match
 *
 */

+ (void)removeAnyResponseError;



/*
 *  @abstract   Calls the block when ever a new http request is made
 *
 *  @discussion This function becomes usefull in BDD so that you can test
 *              that button A results in a http request with the correct URL
 *
 */

+ (void)URLRequested:(URLRequestedBlock)block;

@end
