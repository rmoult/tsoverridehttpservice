//
//  Created by Richard Moult on 08/07/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import "TSRawHTTPFile.h"


@interface TSRawHTTPFile ()
@end


@implementation TSRawHTTPFile


- (id)initWithFileName:(NSString *)filename ofType:(NSString *)type {

    self = [super init];

    if (self) {

        NSString * rawHttpString = [self stringContentsOfFileNamed:filename ofType:type];

        NSLog(@">> app rawHttpString %@", rawHttpString);

        _rawHttpList = [self listFromRawHttp:rawHttpString];
    }

    return self;
}


- (NSString *)stringContentsOfFileNamed:(NSString *)fileName ofType:(NSString *)ofType {

    if (!fileName || !ofType) {

        return nil;
    }

    NSBundle * bundle = [NSBundle bundleForClass:[self class]];

    NSString * filePath = [bundle pathForResource:fileName ofType:ofType];

    NSLog(@">>> app filePath %@", filePath);

    NSData * fileData = [NSData dataWithContentsOfFile:filePath];

    return [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
}


- (NSArray *)listFromRawHttp:(NSString *)rawHttp {

    if (!rawHttp) {

        return nil;
    }

    return [rawHttp componentsSeparatedByString:@"\n"];
}


- (NSHTTPURLResponse *)httpURLResponseForURL:(NSURL *)URL {

    NSString *HTTPVersion = [self extractHTTPVersion];

    NSInteger statusCode = [self extractStatusCode];

    NSDictionary *headerDictionary = [self extractHeaders];

    return [[NSHTTPURLResponse alloc] initWithURL:URL
                                       statusCode:statusCode
                                      HTTPVersion:HTTPVersion
                                     headerFields:headerDictionary];
}


- (NSDictionary *)extractHeaders {

    if (!self.rawHttpList || self.rawHttpList.count <= 0) {

        return nil;
    }

    NSMutableDictionary * headerDictionary = [NSMutableDictionary new];

    for (int i=1; i<self.rawHttpList.count; i++)
    {
        NSString * line = self.rawHttpList[i];

        NSRange range = [line rangeOfString:@":"];

        if (range.location != NSNotFound)
        {
            NSString * key = [line substringWithRange:NSMakeRange(0, range.location)];

            NSString * value = [line substringWithRange:NSMakeRange(range.location+1, line.length-(range.location+1))];

            NSString * trimmedValue = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

            [headerDictionary setObject:trimmedValue forKey:key];
        }
        else
        {
            break;
        }
    }
    return headerDictionary;
}


- (NSString *)extractHTTPVersion {

    if (!self.rawHttpList || self.rawHttpList.count < 1) {

        return @"HTTP/1.1";
    }

    NSArray * firstLineList = [self.rawHttpList[0] componentsSeparatedByString:@" "];

    if (firstLineList && firstLineList.count > 2) {

        return firstLineList[0];
    }

    return @"HTTP/1.1";
}


- (NSInteger)extractStatusCode {

    if (!self.rawHttpList || self.rawHttpList.count <= 0) {

        return NSNotFound;
    }

    NSArray * firstLineList = [self.rawHttpList[0] componentsSeparatedByString:@" "];

    if (firstLineList && firstLineList.count > 2) {

        return [firstLineList[1] integerValue];
    }

    return NSNotFound;
}


- (NSData *)extractBody {

    if (!self.rawHttpList || self.rawHttpList.count <= 0) {

        return nil;
    }

    BOOL startAppending = NO;

    NSMutableString *string = [NSMutableString new];

    for (int i=0; i<self.rawHttpList.count; i++) {

        NSString * line = self.rawHttpList[i];

        if (line.length <= 0) {

            startAppending = YES;

            continue;
        }

        if (startAppending) {

            [string appendString:line];
        }
    }

    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

@end
