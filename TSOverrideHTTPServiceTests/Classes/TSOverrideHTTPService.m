//
//  CannedURLProtocol.m
//  grid
//
//  Created by Richard Moult on 24/07/2013.
//  Copyright (c) 2013 trickysquirrel. All rights reserved.
//

#import "TSOverrideHTTPService.h"
#import "TSOverrideHTTPServicePODO.h"
#import "TSRawHTTPFile.h"



@interface TSOverrideHTTPService()
@property (nonatomic, strong) NSMutableArray *podoMappingList;
@property (nonatomic, strong) TSOverrideHTTPServicePODO *anyErrorResponsePODO;
@property (nonatomic, strong) URLRequestedBlock httpURLRequestedBlock;
@end



@implementation TSOverrideHTTPService


#pragma mark - Static public functions


+ (void)setUp {

    [NSURLProtocol registerClass:[TSOverrideHTTPService class]];
}


+ (void)tearDown {

    [[TSOverrideHTTPService sharedInstance] tearDown];
    [NSURLProtocol unregisterClass:[TSOverrideHTTPService class]];
}


+ (void)setUpAnyResponsesWithError:(NSError *)error {

    [[TSOverrideHTTPService sharedInstance] setUpAnyResponsesWithError:error];
}


+ (void)removeAnyResponseError {

    [[TSOverrideHTTPService sharedInstance] removeAnyResponseError];
}


+ (void)setUpRequestRegex:(NSString *)regex mappedToResponseError:(NSError *)error {

    [[TSOverrideHTTPService sharedInstance] setUpRequestURLRegex:regex mappedToResponseError:error];
}


+ (void)setUpRequestURLRegex:(NSString *)regex mappedToRawHttpFileNamed:(NSString *)fileName ofType:(NSString *)ofType {
    
    [[TSOverrideHTTPService sharedInstance] setUpRequestRegex:regex mappedToResponseError:fileName ofType:ofType];
}


+ (void)removeMappedResponseForRequestURLRegex:(NSString *)regex {

    [[TSOverrideHTTPService sharedInstance] removeMappedResponseForURLRegex:regex];
}


+ (void)URLRequested:(URLRequestedBlock)block {

    [[TSOverrideHTTPService sharedInstance] URLRequested:block];
}


#pragma mark - Static private functions


+ (TSOverrideHTTPService *)sharedInstance {

    static TSOverrideHTTPService * sharedInstance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedInstance = [[TSOverrideHTTPService alloc] init];
    });

    return sharedInstance;
}


#pragma mark - life cycle


- (id)init {

    self = [super init];

    if (self) {

        _podoMappingList = [NSMutableArray new];
    }
    return self;
}


- (void)tearDown {

    self.anyErrorResponsePODO = nil;
    [self removeAllPODOsFromList];
}


#pragma mark - URL Request block


- (void)URLRequested:(URLRequestedBlock)block {

    self.httpURLRequestedBlock = block;
}


#pragma mark - Set up files mapped to URLs


- (void)setUpRequestRegex:(NSString *)regex mappedToResponseError:(NSString *)fileName ofType:(NSString *)ofType {

    TSOverrideHTTPServicePODO *podo = [self createFileHTTPServicePODOWithRegex:regex fileName:fileName fileType:ofType];
    [self addPODOToList:podo];
}



#pragma mark - Set up response errors


- (void)setUpAnyResponsesWithError:(NSError *)error {

    TSOverrideHTTPServicePODO *podo = [self createErrorHTTPServicePODOWithRegex:nil error:error];
    self.anyErrorResponsePODO = podo;
}


- (void)setUpRequestURLRegex:(NSString *)regex mappedToResponseError:(NSError *)error {

    TSOverrideHTTPServicePODO *podo = [self createErrorHTTPServicePODOWithRegex:regex error:error];
    [self addPODOToList:podo];
}


- (void)removeAnyResponseError {

    self.anyErrorResponsePODO = nil;
}


#pragma mark - Overriding functions for network responses


+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {

    return request;
}


+ (BOOL)canInitWithRequest:(NSURLRequest *)request {

    TSOverrideHTTPService *mockedService = [TSOverrideHTTPService sharedInstance];
    [mockedService callURLRequestedBlockWithRequest:request];
    return [mockedService shouldOverrideRequestURL:request.URL];
}


- (void)callURLRequestedBlockWithRequest:(NSURLRequest *)request {

    if (self.httpURLRequestedBlock) {
        self.httpURLRequestedBlock(request);
    }
}


- (BOOL)shouldOverrideRequestURL:(NSURL *)URL {

    if ([self httpServicePODOForURL:URL]) {
        return YES;
    }
    return NO;
}


- (void)startLoading {

    id client = [self client];
    TSOverrideHTTPService *sharedService = [TSOverrideHTTPService sharedInstance];
    [sharedService setClientResponse:client forURL:self.request.URL];
}


- (void)stopLoading {

}


- (void)setClientResponse:(id)client forURL:(NSURL *)URL {

    TSOverrideHTTPServicePODO *HTTPServicePODO = [self httpServicePODOForURL:URL];

    if (HTTPServicePODO.error) {

        [self setClient:client error:HTTPServicePODO.error];
    }
    else if(HTTPServicePODO.fileName && HTTPServicePODO.fileType) {

        [self setClient:client responseForFileName:HTTPServicePODO.fileName ofType:HTTPServicePODO.fileType];
    }
}


#pragma mark - set up client response


- (void)setClient:(id)client error:(NSError *)error {

    [client URLProtocol:self didFailWithError:error];
}


- (void)setClient:(id)client responseForFileName:(NSString *)fileName ofType:(NSString *)ofType {

    TSRawHTTPFile *rawHttpFile = [[TSRawHTTPFile alloc] initWithFileName:fileName ofType:ofType];
    [self setClient:client withRawHttp:rawHttpFile];
}


- (void)setClient:(id)client withRawHttp:(TSRawHTTPFile *)rawHttp {

    NSHTTPURLResponse *response = [rawHttp httpURLResponseForURL:self.request.URL];
    NSData *responseData = [rawHttp extractBody];

    [client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
    [client URLProtocol:self didLoadData:responseData];
    [client URLProtocolDidFinishLoading:self];
}


#pragma mark - podo helpers


- (void)removeMappedResponseForURLRegex:(NSString *)regex {

    TSOverrideHTTPServicePODO *podo = [self podoContainingRegex:regex];

    if (podo) {

        [self removePODOFromList:podo];
        [self removeMappedResponseForURLRegex:regex];
    }
}


- (TSOverrideHTTPServicePODO *)createErrorHTTPServicePODOWithRegex:(NSString *)regex error:(NSError *)error {

    TSOverrideHTTPServicePODO *podo = [TSOverrideHTTPServicePODO new];
    podo.regex = regex;
    podo.error = error;
    return podo;
}


- (TSOverrideHTTPServicePODO *)createFileHTTPServicePODOWithRegex:(NSString *)regex fileName:(NSString *)fileName fileType:(NSString *)fileType {

    TSOverrideHTTPServicePODO *podo = [TSOverrideHTTPServicePODO new];
    podo.regex = regex;
    podo.fileName = fileName;
    podo.fileType = fileType;
    return podo;
}


#pragma mark - podo Mapping List helpers


- (TSOverrideHTTPServicePODO *)httpServicePODOForURL:(NSURL *)URL {

    if (self.anyErrorResponsePODO) {

        return self.anyErrorResponsePODO;
    }

    @synchronized(self.podoMappingList) {

        return [self matchingURL:URL];
    }
}


- (TSOverrideHTTPServicePODO *)matchingURL:(NSURL *)URL {

    @synchronized(self.podoMappingList) {

        for (TSOverrideHTTPServicePODO *podo in self.podoMappingList) {

            if (podo.regex) {

                NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", podo.regex];
                if ( [urlTest evaluateWithObject:URL.absoluteString]) {

                    return podo;
                }

            }
        }
    }

    return nil;
}


- (TSOverrideHTTPServicePODO *)podoContainingRegex:(NSString *)regex {

    @synchronized(self.podoMappingList) {

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.regex contains[cd] %@", regex];

        NSArray *result = [self.podoMappingList filteredArrayUsingPredicate:predicate];

        if (result.count > 0) {
            return [result firstObject];
        }
    }

    return nil;
}


- (void)addPODOToList:(TSOverrideHTTPServicePODO *)podo {

    @synchronized(self.podoMappingList) {

        [self.podoMappingList addObject:podo];
    }
}


- (void)removePODOFromList:(TSOverrideHTTPServicePODO *)podo {

    @synchronized(self.podoMappingList) {

        [self.podoMappingList removeObject:podo];
    }
}


- (void)removeAllPODOsFromList {

    @synchronized(self.podoMappingList) {

        [self.podoMappingList removeAllObjects];
    }
}


@end
