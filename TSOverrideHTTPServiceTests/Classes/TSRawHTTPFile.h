//
//  Created by Richard Moult on 08/07/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TSRawHTTPFile : NSObject

@property (nonatomic, strong, readonly) NSArray *rawHttpList;

- (id)initWithFileName:(NSString *)filename ofType:(NSString *)type;

- (NSHTTPURLResponse *)httpURLResponseForURL:(NSURL *)URL;

- (NSData *)extractBody;

@end
