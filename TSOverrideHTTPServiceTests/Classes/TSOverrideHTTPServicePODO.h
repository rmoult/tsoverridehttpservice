//
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSOverrideHTTPServicePODO : NSObject

@property (nonatomic, strong) NSString *regex;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSString *fileType;

@end
