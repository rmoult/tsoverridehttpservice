//
//  TSOverrideHTTPServiceTests.m
//  TSOverrideHTTPServiceTests
//
//  Created by Richard Moult on 09/07/2015.
//  Copyright © 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "TSOverrideHTTPService.h"



@interface TSOverrideHTTPServiceTests : XCTestCase
@property (nonatomic, strong) NSString *dummyURLString;
@property (nonatomic, strong) NSURL *dummyURL;
@property (nonatomic, strong) NSOperationQueue *queue;
@end



@implementation TSOverrideHTTPServiceTests


- (void)setUp {

    [super setUp];

    self.dummyURLString = @"http://xyz123sawdrfr";

    self.dummyURL = [NSURL URLWithString:self.dummyURLString];

    self.queue = [NSOperationQueue mainQueue];

    NSLog(@"Env = %@", [[NSBundle bundleForClass:[self class]] objectForInfoDictionaryKey:@"WEB_SERVICE_BASE_URL"]);
}


- (void)tearDown {

    [super tearDown];
}


#pragma mark - set up / tear down


- (void)testMockHTTPService_setUp_registersNSURLProtocolClass {

    id mockNSURLProtocol = [OCMockObject niceMockForClass:[NSURLProtocol class]];

    [[[mockNSURLProtocol expect] classMethod] registerClass:[TSOverrideHTTPService class]];

    [TSOverrideHTTPService setUp];

    XCTAssertNoThrow([mockNSURLProtocol verify]);

    [mockNSURLProtocol stopMocking];
}


- (void)testMockHTTPService_tearDown_unregistersNSURLProtocolClass {

    id mockNSURLProtocol = [OCMockObject niceMockForClass:[NSURLProtocol class]];

    [[[mockNSURLProtocol expect] classMethod] unregisterClass:[TSOverrideHTTPService class]];

    [TSOverrideHTTPService tearDown];

    XCTAssertNoThrow([mockNSURLProtocol verify]);

    [mockNSURLProtocol stopMocking];
}


#pragma mark - response errors


- (void)testSetUpAllResponsesWithError_setError_resturnsExpectedError {

    NSError *expectedError = [self fakeError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpAnyResponsesWithError:expectedError];

    NSError *returnedError = nil;

    (void)[self requestDummyURLWithReturningError:&returnedError];

    XCTAssertEqualObjects(returnedError.domain, expectedError.domain);
    XCTAssertEqual(returnedError.code, expectedError.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpURLWithResponseError_setCorrectURL_returnsExpectedError {

    NSError *expectedError = [self fakeError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestRegex:self.dummyURLString mappedToResponseError:expectedError];

    NSError *returnedError = nil;

    (void)[self requestDummyURLWithReturningError:&returnedError];

    XCTAssertEqualObjects(returnedError.domain, expectedError.domain);
    XCTAssertEqual(returnedError.code, expectedError.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpURLWithResponseError_set2DifferentCorrectURL_returnsExpectedErrorForEachURL {

    NSURL *URL1 = [NSURL URLWithString:@"bob"];
    NSURL *URL2 = [NSURL URLWithString:@"fred"];

    NSError *expectedError1 = [self fakeError];
    NSError *expectedError2 = [self madeUpError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestRegex:URL1.absoluteString mappedToResponseError:expectedError1];
    [TSOverrideHTTPService setUpRequestRegex:URL2.absoluteString mappedToResponseError:expectedError2];

    NSError *returnedError1 = nil;
    NSError *returnedError2 = nil;

    (void)[self requestURL:URL1 error:&returnedError1];
    (void)[self requestURL:URL2 error:&returnedError2];

    XCTAssertEqualObjects(returnedError1.domain, expectedError1.domain);
    XCTAssertEqual(returnedError1.code, expectedError1.code);

    XCTAssertEqualObjects(returnedError2.domain, expectedError2.domain);
    XCTAssertEqual(returnedError2.code, expectedError2.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpURLWithResponseError_setInCorrectURL_returnsUnExpectedError {

    NSError *expectedError = [self madeUpError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestRegex:self.dummyURLString mappedToResponseError:expectedError];

    NSError *returnedError = nil;

    (void)[self requestURL:[NSURL URLWithString:@"http://bob"] error:&returnedError];

    XCTAssertNotEqual(returnedError.code, expectedError.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpAllURLResponseError_setSpecificURLErrorButReturnAllErrors_returnsExpectedError {

    NSError *expectedError = [self fakeError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpAnyResponsesWithError:expectedError];

    NSError *returnedError = nil;

    (void)[self requestURL:[NSURL URLWithString:@"http://bob"] error:&returnedError];

    XCTAssertEqualObjects(returnedError.domain, expectedError.domain);
    XCTAssertEqual(returnedError.code, expectedError.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testRemoveResponseErrorForURL_removeSetURLErrorResponse_returnsDifferentError {

    NSError *expectedError = [self fakeError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestRegex:self.dummyURL.absoluteString mappedToResponseError:expectedError];

    [TSOverrideHTTPService removeMappedResponseForRequestURLRegex:self.dummyURL.absoluteString];

    NSError *returnedError = nil;

    (void)[self requestURL:self.dummyURL error:&returnedError];

    XCTAssertNotEqualObjects(returnedError.domain, expectedError.domain);
    XCTAssertNotEqual(returnedError.code, expectedError.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testRemoveAnyResponseError_setAnyErrorThenRemove_shouldNotReturnAnyError {

    NSError *expectedError = [self fakeError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpAnyResponsesWithError:expectedError];

    [TSOverrideHTTPService removeAnyResponseError];

    NSError *returnedError = nil;

    (void)[self requestURL:[NSURL URLWithString:@"http://bob"] error:&returnedError];

    XCTAssertNotEqualObjects(returnedError.domain, expectedError.domain);
    XCTAssertNotEqual(returnedError.code, expectedError.code);

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpMappedRegexResponseError_correctRegex_returnsCorrectError {

    NSError *expectedError1 = [self fakeError];
    NSError *expectedError2 = [self madeUpError];

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestRegex:@"(http|https)://example/limit=\\d{1,}&offset=2" mappedToResponseError:expectedError1];

    [TSOverrideHTTPService setUpRequestRegex:@"(http|https)://example/limit=\\d{1,}&offset=3" mappedToResponseError:expectedError2];

    NSError *returnedError1 = nil;
    NSError *returnedError2 = nil;

    (void)[self requestURL:[NSURL URLWithString:@"http://example/limit=200&offset=2"] error:&returnedError1];
    (void)[self requestURL:[NSURL URLWithString:@"http://example/limit=200&offset=3"] error:&returnedError2];

    XCTAssertEqualObjects(returnedError1.domain, expectedError1.domain);
    XCTAssertEqual(returnedError1.code, expectedError1.code);
    XCTAssertEqualObjects(returnedError2.domain, expectedError2.domain);
    XCTAssertEqual(returnedError2.code, expectedError2.code);

    [TSOverrideHTTPService tearDown];
}



#pragma mark - response files


- (void)testSetUpRequestURLMappedToResponseFromRawHttpFileNamed_returnsCorrectFileData {

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestURLRegex:self.dummyURL.absoluteString mappedToRawHttpFileNamed:@"response200" ofType:@"rawHttp"];

    NSData *data = [self requestURL:self.dummyURL error:nil];

    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

    XCTAssertEqualObjects(dataString, @"responsebody");

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpRequestURLMappedToResponseFromRawHttpFileNamed_reponse200File_returnsCorrectHTTPHeaders {

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestURLRegex:self.dummyURL.absoluteString mappedToRawHttpFileNamed:@"response200" ofType:@"rawHttp"];

    NSHTTPURLResponse *response = nil;

    (void)[self requestURL:self.dummyURL error:nil response:&response];

    XCTAssertEqual(response.statusCode, 200);
    XCTAssertEqualObjects(response.allHeaderFields[@"Expires"], @"Mon, 29 Jul 2013 13:17:35 GMT");
    XCTAssertEqualObjects(response.allHeaderFields[@"ETag"], @"\"0965829f90744148b0da512c8cdfb33e9\"");
    XCTAssertEqualObjects(response.allHeaderFields[@"Cache-Control"], @"max-age=3600");
    XCTAssertEqualObjects(response.allHeaderFields[@"Vary"], @"Accept-Encoding");
    XCTAssertEqualObjects(response.allHeaderFields[@"Server"], @"Apache/2.2.17 (Unix) mod_ssl/2.2.17 OpenSSL/0.9.8e-fips-rhel5 mod_jk/1.2.28");
    XCTAssertEqualObjects(response.allHeaderFields[@"Content-Language"], @"en-US");
    XCTAssertEqualObjects(response.allHeaderFields[@"Content-Type"], @"application/json;charset=UTF-8");
    XCTAssertEqualObjects(response.allHeaderFields[@"X-Cache-Lookup"], @"HIT from P-A-4:3128");
    XCTAssertEqualObjects(response.allHeaderFields[@"Content-Encoding"], @"gzip");
    XCTAssertEqualObjects(response.allHeaderFields[@"Content-Length"], @"6873");
    XCTAssertEqualObjects(response.allHeaderFields[@"Date"], @"Mon, 29 Jul 2013 12:18:58 GMT");
    XCTAssertEqualObjects(response.allHeaderFields[@"Connection"], @"keep-alive");

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpRequestURLMappedToResponseFromRawHttpFileNamed_response503File_returnsCorrectHTTPHeaders {

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestURLRegex:self.dummyURL.absoluteString mappedToRawHttpFileNamed:@"response503" ofType:@"rawHttp"];

    NSHTTPURLResponse *response = nil;

    (void)[self requestURL:self.dummyURL error:nil response:&response];

    XCTAssertEqual(response.statusCode, 503);

    [TSOverrideHTTPService tearDown];
}


- (void)testRemoveMappedResponseFileForRequestURL_addedAndThenRemovedURL_shouldNotReturnMappedResponse {

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestURLRegex:self.dummyURL.absoluteString mappedToRawHttpFileNamed:@"response200" ofType:@"rawHttp"];

    [TSOverrideHTTPService removeMappedResponseForRequestURLRegex:self.dummyURL.absoluteString];

    NSHTTPURLResponse *response = nil;

    (void)[self requestURL:self.dummyURL error:nil response:&response];

    XCTAssertNotEqual(response.statusCode, 200);
    XCTAssertNotEqualObjects(response.allHeaderFields[@"Expires"], @"Mon, 29 Jul 2013 13:17:35 GMT");
    XCTAssertNotEqualObjects(response.allHeaderFields[@"ETag"], @"\"0965829f90744148b0da512c8cdfb33e9\"");

    [TSOverrideHTTPService tearDown];
}


- (void)testSetUpMappedRegexResponseFileForRequestURL_correctRegex_returnsCorrectError {

    [TSOverrideHTTPService setUp];

    [TSOverrideHTTPService setUpRequestURLRegex:@"(http|https)://example/limit=\\d{1,}&offset=2" mappedToRawHttpFileNamed:@"response200" ofType:@"rawHttp"];

    [TSOverrideHTTPService setUpRequestURLRegex:@"(http|https)://example/limit=\\d{1,}&offset=3" mappedToRawHttpFileNamed:@"response503" ofType:@"rawHttp"];

    NSHTTPURLResponse *response1 = nil;
    NSHTTPURLResponse *response2 = nil;

    (void)[self requestURL:[NSURL URLWithString:@"http://example/limit=200&offset=2"] error:nil response:&response1];
    (void)[self requestURL:[NSURL URLWithString:@"http://example/limit=200&offset=3"] error:nil response:&response2];

    XCTAssertEqual(response1.statusCode, 200);
    XCTAssertEqual(response2.statusCode, 503);

    [TSOverrideHTTPService tearDown];
}


#pragma mark - delegate

- (void)testURLWatcher_URLRequest_callsBlockWithURL {

    __block BOOL blockWithCorrectURLCalled = NO;

    [TSOverrideHTTPService setUp];

    NSHTTPURLResponse *response = nil;

    [TSOverrideHTTPService URLRequested:^(NSURLRequest *URLRequest) {

        if ([URLRequest.URL.absoluteString isEqualToString:self.dummyURLString]) {
            blockWithCorrectURLCalled = YES;
        }
    }];

    (void)[self requestURL:self.dummyURL error:nil response:&response];

    XCTAssertTrue(blockWithCorrectURLCalled);

    [TSOverrideHTTPService tearDown];
}


#pragma mark - helpers


- (NSError *)fakeError {

    return [NSError errorWithDomain:@"fakeError" code:98765 userInfo:nil];
}


- (NSError *)madeUpError {

    return [NSError errorWithDomain:@"madeUpDomain" code:12345 userInfo:nil];
}


- (NSData *)requestDummyURLWithReturningError:(NSError **)error {

    return [self requestURL:[NSURL URLWithString:self.dummyURLString] error:error];
}


- (NSData *)requestURL:(NSURL *)URL error:(NSError **)error {

    NSHTTPURLResponse *response = nil;

    return [self requestURL:URL error:error response:&response];
}


- (NSData *)requestURL:(NSURL *)URL error:(NSError **)error response:(NSHTTPURLResponse **)response {

    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:URL
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                    timeoutInterval:1.0];


    return [NSURLConnection sendSynchronousRequest:request returningResponse:response error:error];
}


@end
