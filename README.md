# TSOverrideHTTPService

Allows for an easy way for a developer to substitute HTTP responses during tests.

# Pod install
```
pod 'TSOverrideHTTPService', :git => 'https://bitbucket.org/rmoult/tsoverridehttpservice.git'
```

# Import Manually
Download the project and include all the files inside TSOverrideHTTPServiceTests/Classes into your own test target 


# How To Use
For this class to override the network responses for testing purposes you will first need to call

```
[TSOverrideHTTPService setUp];
```
Followed by any other static functions in this class to set up the responses

```
[TSOverrideHTTPService setUpRequestRegex:@"(http|https)://example/limit=\\d{1,}&offset=2" mappedToResponseError:error];

[TSOverrideHTTPService setUpRequestURLRegex:@"(http|https)://example/limit=\\d{1,}&offset=2" mappedToRawHttpFileNamed:@"response200" ofType:@"rawHttp"];
```
where the raw file would be
```
HTTP/1.1 200 OK
Expires: Mon, 29 Jul 2013 13:17:35 GMT
ETag: "0965829f90744148b0da512c8cdfb33e9"
Cache-Control: max-age=3600
Vary: Accept-Encoding
Server: Apache/2.2.17 (Unix) mod_ssl/2.2.17 OpenSSL/0.9.8e-fips-rhel5 mod_jk/1.2.28
Content-Language: en-US
Content-Type: application/json;charset=UTF-8
X-Cache-Lookup: HIT from P-A-4:3128
Content-Encoding: gzip
Content-Length: 6873
Date: Mon, 29 Jul 2013 12:18:58 GMT
Connection: keep-alive

responsebody
```

Given that an error and http files can be mapped to the same URL the first one set gets precedence

Don't forget to disable this behaviour once the test have been completed

```
[TSOverrideHTTPService tearDown];
```
Its good practice to call the class setUp/tearDown functions in the test setUp and tearDown methods
