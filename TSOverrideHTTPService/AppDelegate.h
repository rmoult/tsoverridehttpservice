//
//  AppDelegate.h
//  TSOverrideHTTPService
//
//  Created by Richard Moult on 09/07/2015.
//  Copyright © 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

