//
//  AppDelegate.m
//  TSOverrideHTTPService
//
//  Created by Richard Moult on 09/07/2015.
//  Copyright © 2015 TrickySquirrel. All rights reserved.
//

#import "AppDelegate.h"
#import "TSOverrideHTTPService.h"



@interface AppDelegate ()

@end



@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
