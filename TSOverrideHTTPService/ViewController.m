//
//  ViewController.m
//  TSOverrideHTTPService
//
//  Created by Richard Moult on 09/07/2015.
//  Copyright © 2015 TrickySquirrel. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *responseCodeLabel;

@end



@implementation ViewController


- (IBAction)sendDataButtonPressed:(id)sender {

    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://google.com"]
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                    timeoutInterval:1.0];

    NSError *error = nil;
    NSHTTPURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

    if (data) {

        self.responseCodeLabel.text = [NSString stringWithFormat:@"%ld", (long)response.statusCode];
    }
    else {

        self.responseCodeLabel.text = error.localizedDescription;
    }

}

@end
