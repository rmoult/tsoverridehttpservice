//
//  TSOverrideHTTPServiceUITests.m
//  TSOverrideHTTPServiceUITests
//
//  Created by Richard Moult on 09/07/2015.
//  Copyright © 2015 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import "TSOverrideHTTPService.h"




@interface TSOverrideHTTPServiceUITests : XCTestCase

@end



@implementation TSOverrideHTTPServiceUITests


- (void)setUp {

    [super setUp];

    self.continueAfterFailure = NO;

    [[[XCUIApplication alloc] init] launch];
}


- (void)tearDown {

    [super tearDown];
}


//- (void)testSelectingButton_doesSomething {
//
//    XCUIApplication *app = [[XCUIApplication alloc] init];
//    [app.buttons[@"send data"] tap];
//    [app.staticTexts[@"200"] tap];
//    
//    
//    
//}

//- (void)testSendDataButon_reponseCode123_showStatusCode123 {

//    NSLog(@">>> %@", [NSBundle bundleForClass:[self class]]);
//
//    NSString *pathOfResponse503 = [[NSBundle bundleForClass:[self class]] pathForResource:@"response503" ofType:@"rawHttp"];
//
//    NSString *pathOfResponse200 = [[NSBundle bundleForClass:[self class]] pathForResource:@"response200" ofType:@"rawHttp"];
//
//    NSData *response503Data = [self stringContentsOfFileNamed:@"response503" ofType:@"rawHttp"];
//
//    NSString *response503 = [[NSString alloc] initWithData:response503Data encoding:NSUTF8StringEncoding];
//
//    NSLog(@">>> %@", [[NSBundle bundleForClass:[self class]] pathForResource:@"response503" ofType:@"rawHttp"]);
//
//    [response503 writeToFile:pathOfResponse200 atomically:YES encoding:NSUTF8StringEncoding error:nil];
//
//
//    XCUIApplication *app = [[XCUIApplication alloc] init];
//
//
//
//  //  id application = [[UIApplication sharedApplication] delegate];
//
//    [self abcFileNamed:@"response200" ofType:@"rawHttp"];
//
//    NSLog(@">>> test path %@", [self filePathForBundle:[app class] fileNamed:@"response200" ofType:@"rawHttp"]);
//
//    [app.buttons[@"send data"] tap];
//    //[app.staticTexts[@"response code"] tap];
//}


//- (void)abcFileNamed:(NSString *)fileName ofType:(NSString *)ofType {
//
//    NSBundle *bundle = [NSBundle bundleWithPath:@"(TARGET_BUILD_DIR)"];
//
//    NSString *string = [bundle pathForResource:fileName ofType:ofType];
//
//    NSLog(@"string %@", string);
//}
//
//
//
//
//- (NSData *)stringContentsOfFileNamed:(NSString *)fileName ofType:(NSString *)ofType {
//
//    if (!fileName || !ofType) {
//
//        return nil;
//    }
//
//    NSBundle * bundle = [NSBundle bundleForClass:[self class]];
//
//    NSString * filePath = [bundle pathForResource:fileName ofType:ofType];
//
//    return [NSData dataWithContentsOfFile:filePath];
//}
//
//
//- (NSString *)filePathForFileNamed:(NSString *)fileName ofType:(NSString *)ofType {
//
//    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
//
//    return [bundle pathForResource:fileName ofType:ofType];
//}
//
//
//- (NSString *)filePathForBundle:(Class)class fileNamed:(NSString *)fileName ofType:(NSString *)ofType {
//
//    NSBundle *bundle = [NSBundle bundleForClass:class];
//
//    return [bundle pathForResource:fileName ofType:ofType];
//}

@end
